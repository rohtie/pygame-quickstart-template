import pygame
from pygame import *

from settings import *


class Animation(pygame.sprite.Sprite):
    def __init__(self, frames, speed=10, loop=True):
        self.frames = frames

        self.loop = loop
        self.frame = 0
        self.counter = 0
        self.speed = speed
        self.done = False

    def next(self):
        """Return next frame."""
        if self.loop or self.frame < len(self.frames) - 1:
            self.advance()
        else:
            self.done = True

        return self.frames[self.frame]

    def advance(self):
        """Advance frame count without returning."""
        if self.counter % self.speed == 0:
            self.frame += 1

            if self.frame > len(self.frames) - 1:
                self.frame = 0
                self.counter = 0

        self.counter += 1

    def reset(self):
        """Reset animation to first frame."""
        self.frame = 0
        self.counter = 0
