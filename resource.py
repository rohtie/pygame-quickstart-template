import os
import re

import pygame
from pygame import *

from settings import *


class AssetLibrary:
    def __init__(self):
        library = {}

        # Load all assets
        for root, dirnames, filenames in os.walk('assets'):
            for filename in filenames:
                name = (os.path.join(root, filename)
                               .replace('.png', '')
                               .replace('assets/', ''))

                library[name] = self.process_image(
                    os.path.join(root, filename)
                )

        self.library = library

        # Group assets with trailing digits
        group_regex = re.compile(r'(?P<name>[a-z]+)\d+')

        groups = {}
        for filename in library:
            # name, id = group_regex.findall(filename)
            match = group_regex.search(filename)
            if match:
                name = match.group('name')

                if name not in groups:
                    groups[name] = []

                groups[name].append(library[filename])

        self.groups = groups

    def process_image(self, filename):
        """Return loaded and converted image."""
        image = pygame.image.load(filename)
        image.convert()

        if SCALE == 1:
            return image

        else:
            # Scale the image
            w, h = image.get_size()
            return pygame.transform.scale(image, (w * SCALE, h * SCALE))

    def get(self, name):
        """Return asset from the asset library."""
        if name in self.library:
            return self.library[name]

        else:
            print('Asset `{name}` not found'.format(name=name))

    def get_group(self, name):
        """Return asset group."""
        if name in self.groups:
            return self.groups[name]

        else:
            print('Asset `{name}` not found'.format(name=name))
