from pygame import Surface, Color, event, mouse
from pygame.locals import *

from animation import Animation
from resource import AssetLibrary
from sprite import Sprite
import settings

class Game:
    def __init__(self):
        self.running = True

        self.assets = AssetLibrary()

        # Create a 20x20 square
        square = Surface((20, 20))
        square = square.convert()
        square.fill(Color('#ff0000'))
        self.square = square

        # Create new sprite at x=0 y=0
        self.sprite = Sprite(self.assets.get('sprite'), (0, 0))

        # Create new animation
        self.anim = Animation(self.assets.get_group('anim'))

    def handle_event(self, event):
        pass

    def update(self):
        self.sprite.rect.x += 1

    def render(self, screen):
        width, height = settings.DISPLAY

        # Draw square in the middle of the screen
        screen.blit(self.square, (width / 2, height / 2))

        # Draw sprite
        self.sprite.draw(screen)

        # Draw animations
        screen.blit(self.anim.next(), (200, 200))
