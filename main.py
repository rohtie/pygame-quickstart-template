import pygame
from pygame import Surface, Color, time

from game import Game
import settings

def main():
    # Init pygame
    pygame.init()
    pygame.mixer.init()
    timer = pygame.time.Clock()

    # Screen is the main surface, which will be shown
    screen = pygame.display.set_mode(settings.DISPLAY)

    # Load black background
    background = Surface(settings.DISPLAY)
    background = background.convert()
    background.fill(Color('#000000'))

    timer = pygame.time.Clock()

    game = Game()
    while game.running:
        timer.tick(settings.FPS)  # 60 fps
        pygame.display.set_caption(settings.GAME_TITLE + " " + str(timer.get_fps()))

        screen.blit(background, (0, 0))
        game.update()
        game.render(screen)

        for event in pygame.event.get():
            game.handle_event(event)
            handle_quit(event)

        pygame.display.update()

def handle_quit(event):
    if event.type == pygame.QUIT:
        raise(SystemExit, 'QUIT')

    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
            raise(SystemExit, 'Quit')

main()
