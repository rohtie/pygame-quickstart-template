import pygame
from pygame import *

from settings import *


class Sprite(pygame.sprite.Sprite):
    def __init__(self, image, position):
        super(Sprite, self).__init__()

        self.image = image
        self.rect = Rect(position, image.get_size())

    def draw(self, screen):
        screen.blit(self.image, self.rect.topleft)
