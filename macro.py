import pygame
from pygame import *

import settings

def load_sprite(name, convert = True, colorkey = None, multiplier=None):
    """Load a sprite and scale it to twice size."""

    if 'assets' in name:
        image = pygame.image.load(name + ".png")
    else:
        image = pygame.image.load("assets/sprites/" + name + ".png")

    if convert:
        image = image.convert()

    if colorkey:
        image.set_colorkey(colorkey)

    if not multiplier:
        multiplier = settings.TILE_MULTIPLIER

    w,h = image.get_size()
    # return image
    return pygame.transform.scale(image, (
        w * multiplier,
        h * multiplier
    ))

def play_music(name):
    """Load music and loop indefinetely."""

    if settings.music:
        pygame.mixer.music.load("assets/music/" + name + ".ogg")
        pygame.mixer.music.play(-1)